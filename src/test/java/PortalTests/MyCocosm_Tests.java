package PortalTests;

import PortalFramework.Driver;
import PortalFramework.Pages.LoginPage;
import PortalFramework.Pages.MainPage;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Random;

/**
 * Created by rreiss on 9/1/2016.
 */
public class MyCocosm_Tests extends PortalUtilTest {

    @Test
      public void Can_GoToMycocosm() throws Exception {
        MainPage mainPage = new MainPage();
   //     mainPage.goTo();  // use this for production url
        mainPage.goToTestSite();
        LoginPage loginPage = new LoginPage();
        loginPage.goTo();
       loginPage.loginAsAdmin();
       // loginPage.loginAsUser();  //to test regular user
        Driver.waitSeconds(2);
        mainPage.goToMycocosm();
    }

    @Test
    //this test will got to the Mycocosm Web Page and traverse through the tree - checking that links for
    //search, BLAST,Clusters, Tree and Nominate are there for each one.  A warning message will be printed in the
    //console if one or more of the links is not found.
    public void Can_accessNodesInMycocosm() throws Exception
    {
        int problemsFound = 0;

        try {
            Can_GoToMycocosm();  // make sure you can get to pager
        } catch (Exception e) {
            System.out.println("**** Error - cannot access page *** ");
            problemsFound++;
            e.printStackTrace();
        }

        List<WebElement> linkNames = Driver.instance.findElements(By.xpath("//*[@id='Map3']/area"));
        int linkNum = linkNames.size();
        System.out.println("the number of linked organisms are " + linkNum);
        Driver.waitSeconds(3);

        for (int i = 1; i < linkNum; i++)  // starting at 1 instead of 0, because first one is video
        {
           linkNames.get(i).click();
           Driver.waitSeconds(4);
            WebElement name= Driver.instance.findElement(By.xpath("//p[contains(@class, 'popupMenu')]/a[2]"));
            String orgName = name.getText();
           System.out.println("Clicked on  Link name " + i + " " + orgName);

            try {
             System.out.println ("  found " + Driver.instance.findElement(By.xpath("//div[contains(text(),'Search')]")).getText());
            } catch (Exception e) {
                System.out.println("**** Warning  NO SEARCH link");
                problemsFound++;
            }

            try {
            System.out.println ("  found " + Driver.instance.findElement(By.xpath("//div[contains(text(),'BLAST')]")).getText());
            } catch (Exception e) {
                System.out.println("**** Warning  NO BLAST link");
                problemsFound++;
            }
            try {
            System.out.println ("  found " + Driver.instance.findElement(By.xpath("//div[contains(text(),'Clusters')]")).getText());
            } catch (Exception e) {
                System.out.println("**** Warning  NO CLUSTERS link");
                problemsFound++;
            }

            try {
                System.out.println("  found " + Driver.instance.findElement(By.xpath("//div[contains(text(),'Tree')]")).getText());
            } catch (Exception e) {
                System.out.println("**** Warning  NO TREE link");
                problemsFound++;
            }

            try {
            System.out.println ("  found " + Driver.instance.findElement(By.xpath("//div[contains(text(),'Nominate')]")).getText());
            } catch (Exception e) {
                System.out.println("**** Warning  NO NOMINATE link");
                problemsFound++;
            }


            Driver.waitSeconds(1);
            //
            //check if popup box has search, blast, clusters and tree, nominame links
           // WebElement search = Driver.instance.findElement(By.name("Search"));
            //WebElement search = Driver.instance.findElement(By.xpath("//div[contains(text(),'click')]"));
           // String textIs = search.getText();
           // System.out.println ("found " + textIs);
            //WebElement search = Driver.instance.findElement(By.xpath("//div[contains(text(),'click')]"));
            // move mouse so popup goes away

            //System.out.println("wait 3 seconds... moving mouse before getting next organism link, look for text at bottom of page");
            WebElement move = Driver.instance.findElement(By.xpath("//p[contains(text(),'click')]"));
            move.click();
            Driver.waitSeconds(1);
        }

        System.out.println("------------------------------------------------------------------------------");
        System.out.println("**** Test Complete!  There were " + problemsFound + " warnings/problems found");
        System.out.println("------------------------------------------------------------------------------");
        Driver.instance.quit();
        Driver.close();
    }


    @Test
    //goes to Mycocosm Web page and randomly selects an organism.
    //click on that organism and popup menu appears.  click on the 7th row.  Should you take
    //you to a page that describes the orgamism
    public void Can_accessOrganismWebPageFromMycocosm() throws Exception {
        int problemsFound = 0;

        try {
            Can_GoToMycocosm();  // make sure you can get to page
        } catch (Exception e) {
            System.out.println("**** Error - cannot access page *** ");
            problemsFound++;
            e.printStackTrace();
        }


        List<WebElement> linkNames = Driver.instance.findElements(By.xpath("//*[@id='Map3']/area"));
        int linkNum = linkNames.size();
        System.out.println("the number of linked organisms are " + linkNum);
        Driver.waitSeconds(3);
        Random rn = new Random();
        int i = rn.nextInt(linkNum);  //5;  // make this random  use this as testcase
        System.out.println("randomly picked organism: " + i);
       // i=17;
        linkNames.get(i).click();
        Driver.waitSeconds(4);
        WebElement name = Driver.instance.findElement(By.xpath("//p[contains(@class, 'popupMenu')]/a[2]"));
        String orgName = name.getText();
        System.out.println("Clicked on  Link name " + i + " " + orgName);
        try {

            // get 7th row - assuming that it is a link to a webpage for an organism (might be a problem for those organisms that do not
            // //have all standard links(search, cluster, tree, etc).
            WebElement linkOrg = Driver.instance.findElement(By.xpath("//*[@id='title']/table/tbody/tr/td/dl/dd[6]/a"));
            System.out.println("  found " + linkOrg.getText());
            linkOrg.click();

        } catch (Exception e) {
            System.out.println("**** Warning  NO  link found");
            problemsFound++;
        }
        Driver.waitSeconds(1);
        System.out.println("------------------------------------------------------------------------------");
        System.out.println("**** Test Complete!  There were " + problemsFound + " warnings/problems found");
        System.out.println("------------------------------------------------------------------------------");
       // Driver.instance.quit();
       // Driver.close();
    }



}
