package PortalTests;

import PortalFramework.Driver;
import PortalFramework.Pages.LoginPage;
import PortalFramework.Pages.MainPage;
import org.junit.Test;

/**
 * Created by rreiss on 9/1/2016.
 */
public class BasicTests extends PortalUtilTest
{
    @Test
    public void Can_goTo_GP_production_URL() throws Exception
    {
        // open up Genome Portal's main page
        MainPage mainPage = new MainPage() ;
        mainPage.goTo();

    }
    @Test
    public void Can_goTo_Beta_URL() throws Exception
    {
        // open up Genome Portal's main page
        MainPage mainPage = new MainPage() ;
        mainPage.goToBetaSite();

    }



    @Test
    public void Can_goTo_GP_test_URL() throws Exception
    {
        // open up Genome Portal's main page (test URL)  as oppose to production URL
        MainPage mainPage = new MainPage() ;
        mainPage.goToTestSite();

    }

    @Test
    public void Can_LoginAsAdmin() throws Exception
{
    MainPage mainPage = new MainPage() ;
    //     mainPage.goTo();  // use this for production url
    mainPage.goToTestSite();
    LoginPage loginPage = new LoginPage() ;
    loginPage.goTo();
    Driver.waitSeconds(1);
    loginPage.loginAsAdmin();
    //
}

    @Test
    public void Can_LoginAsUser() throws Exception
    {
        MainPage mainPage = new MainPage() ;
        //     mainPage.goTo();  // use this for production url
        mainPage.goToTestSite();
        LoginPage loginPage = new LoginPage() ;
        loginPage.goTo();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        loginPage.loginAsUser();
    }

}
