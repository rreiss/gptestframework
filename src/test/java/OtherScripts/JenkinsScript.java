package OtherScripts;

import JenkinsFramework.Driver;
import JenkinsFramework.Pages.*  ;
//LoginPage;
//import JenkinsFramework.Pages.MainPage;
import OtherScripts.ScriptUtilTest;
import org.apache.http.util.Asserts;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

/**
 * Created by rreiss on 9/1/2016.
 */
public class JenkinsScript extends ScriptUtilTest
{
   
    @Test
    public void Deploy_clarityDev() throws Exception
    {
        //open Jenkins and go to the claritydev01 tab
        MainPage mainPage = new MainPage() ;
        mainPage.goTo();
        LoginPage loginPage = new LoginPage() ;
        loginPage.goTo();
        Driver.waitSeconds(1);
        loginPage.loginAsMe();
        Driver.waitSeconds(1);
        ClarityDevPage clarityDevTab = new ClarityDevPage();
       clarityDevTab.goTo();

        // build clarity
        clarityDevTab.goToDeployClaritydev1_clarity() ;
        System.out.println("clarity deployment has started!");
        //go back to claritydev page
        clarityDevTab.goTo();

        //build pluss
        clarityDevTab.goToDeployClaritydev1_pluss();
        System.out.println("pluss deployment has started!");
        //go back to claritydev page
        clarityDevTab.goTo();

        //wait for deployment to complete by checking status,
        System.out.println("wait until both pluss and clarity have finished deployment");
        clarityDevTab.waitForDeployment();
        System.out.println("pluss and clarity deployed!");

        /* verify clarity up and running
           by bringing up clarity login page, if there then OK
        */
         clarityDevTab.checkClarityIsUp();

         //go back to claritydev tab and start regression tests
        mainPage.goTo();
        clarityDevTab.goTo();
        //run regression tests
        clarityDevTab.do_regression_tests();
        System.out.println("Regression Tests started!");
        //go back to claritydev page
        clarityDevTab.goTo();

    }

}
