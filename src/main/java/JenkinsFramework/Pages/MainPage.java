package JenkinsFramework.Pages;

import JenkinsFramework.LoginData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by rreiss on 1/27/2017
 */
public class MainPage
{

     public static String getBaseURL()
     {
       return LoginData.baseURL;
     }


    public  void goTo()
    {
        JenkinsFramework.Driver.instance.get(getBaseURL());
    }




}
