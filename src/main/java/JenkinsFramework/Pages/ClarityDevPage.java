package JenkinsFramework.Pages;

import JenkinsFramework.Driver;
import org.apache.http.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.JavascriptExecutor;

/**
 * Created by rreiss on 9/1/2016.
 */
public class ClarityDevPage   // for jenkins
{

    public  void goTo() {
        WebElement tab =  JenkinsFramework.Driver.instance.findElement(By.linkText("claritydev01"));
        tab.click();
       
     }

    public  void goToDeployClaritydev1_clarity()
    {
        //check if  clarity is currently being built
        WebElement indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_clarity']/td[1]/img "));
        String done =  indicator.getAttribute("alt").toString() ;
        while  (done.equals("In progress") )
        {
            Driver.waitSeconds(30);
            Driver.instance.navigate().refresh();
            indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_pluss']/td[1]/img"));
            done =  indicator.getAttribute("alt").toString() ;
            System.out.println("...waiting");
        }

        if (done.equals("Success") )
            System.out.println("good to go for Clarity build");

        System.out.println("clarity is  " + done);




        String releaseToDeploy =  "release-cla1_2_0";
        WebElement link =  Driver.instance.findElement(By.linkText("deploy_claritydev1_clarity"));
        link.click();
        System.out.println("on claritydevi_clarity page");
        WebElement configure =  Driver.instance.findElement(By.linkText("Configure"));
        configure.click();
        System.out.println("on configure");
        //((JavascriptExecutor)JenkinsFramework.Driver.instance).executeScript("scroll(0,600)");
        Driver.waitSeconds(1);
        Driver.js.executeScript("scroll(0,800)");
        System.out.println("scrolled");
        Driver.waitSeconds(3);

       WebElement branchInput =    Driver.instance.findElement(By.xpath(".//*[@id='main-panel']/form/table/tbody/tr[142]/td[3]/div/div[1]/table/tbody/tr[1]/td[3]/input"));
        if (!branchInput.isDisplayed())   System.out.println(" did not find the branch release input field");
             branchInput.clear();
        branchInput.sendKeys(releaseToDeploy);
        Driver.waitSeconds(3);
        System.out.println("changed the value of field");
        //save configuration
        WebElement button =    Driver.instance.findElement(By.id("yui-gen58-button"));
         button.click();
         //build
        Driver.waitSeconds(3);
        WebElement BuildNow =  Driver.instance.findElement(By.linkText("Build Now"));
        BuildNow.click();
        System.out.println("building clarity");
     
    }



    public  void goToDeployClaritydev1_pluss() {
        //check if  pluss is currently being built
        WebElement indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_pluss']/td[1]/img"));
        String done =  indicator.getAttribute("alt").toString() ;
        while  (done.equals("In progress") )
        {
            Driver.waitSeconds(30);
            Driver.instance.navigate().refresh();
            indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_pluss']/td[1]/img"));
            done =  indicator.getAttribute("alt").toString() ;
            System.out.println("...waiting");
        }

        if (done.equals("Success") )
            System.out.println("good to go for Pluss build");

        System.out.println("pluss is  " + done);

        WebElement link =  Driver.instance.findElement(By.linkText("deploy_claritydev1_pluss"));
        link.click();
       System.out.println("on claritydev_pluss page");
        Driver.waitSeconds(3);
        WebElement BuildNow =  Driver.instance.findElement(By.linkText("Build Now"));
        BuildNow.click();
        System.out.println("building clarity pluss");
    }




    public  void do_regression_tests()
    {

       /*
        //check if  pluss and clarity are still being built.  Wait to start regression test when both builds are completed.


        //check pluss
        WebElement indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_pluss']/td[1]/img"));
        String done =  indicator.getAttribute("alt").toString() ;
        while  (done.equals("In progress") )
        {
            Driver.waitSeconds(30);
            Driver.instance.navigate().refresh();
            indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_pluss']/td[1]/img"));
            done =  indicator.getAttribute("alt").toString() ;
            System.out.println("...waiting for pluss to complete");
        }
        if (done.equals("Success") )
            System.out.println("Pluss has been successfully built");

        //check if  clarity is currently being built
        indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_clarity']/td[1]/img "));
         done =  indicator.getAttribute("alt").toString() ;
        while  (done.equals("In progress") )
        {
            Driver.waitSeconds(30);
            Driver.instance.navigate().refresh();
            indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_pluss']/td[1]/img"));
            done =  indicator.getAttribute("alt").toString() ;
            System.out.println("...waiting for clarity to complete");
        }

        if (done.equals("Success") )
            System.out.println("clarity has been successfully built");
 */
        //now start the regression tests

        WebElement link =  Driver.instance.findElement(By.linkText("Webdriver_regression_test_tube"));
        link.click();
        System.out.println("on Webdriver_regression_test_tube page");
        Driver.waitSeconds(3);
        WebElement BuildNow =  Driver.instance.findElement(By.linkText("Build Now"));
        BuildNow.click();
        System.out.println("Running regression tests");
        

    }


    public  void waitForDeployment()
    {

        //check pluss
        WebElement indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_pluss']/td[1]/img"));
        String done =  indicator.getAttribute("alt").toString() ;
        while  (done.equals("In progress") )
        {
            Driver.waitSeconds(30);
            Driver.instance.navigate().refresh();
            indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_pluss']/td[1]/img"));
            done =  indicator.getAttribute("alt").toString() ;
            System.out.println("...waiting for pluss to complete");
        }
        if (done.equals("Success") )
            System.out.println("Pluss has been successfully built");

        //check if  clarity is currently being built
        indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_clarity']/td[1]/img "));
        done =  indicator.getAttribute("alt").toString() ;
        while  (done.equals("In progress") )
        {
            Driver.waitSeconds(30);
            Driver.instance.navigate().refresh();
            indicator = Driver.instance.findElement(By.xpath(".//*[@id='job_deploy_claritydev1_pluss']/td[1]/img"));
            done =  indicator.getAttribute("alt").toString() ;
            System.out.println("...waiting for clarity to complete");
        }

        if (done.equals("Success") )
            System.out.println("clarity has been successfully built");


    }
    public  void checkClarityIsUp() {

        JenkinsFramework.Driver.instance.get("http://clarity-dev01.jgi-psf.org/");
        Driver.waitSeconds(5);
        try {
            WebElement supportLink = JenkinsFramework.Driver.instance.findElement(By.linkText("Support"));
            System.out.println("clarity is  UP and Running!");
        } catch (Exception e) {
            System.out.println("clarity is  NOT UP - something went wrong!");
            e.printStackTrace();
        }
    }

}
