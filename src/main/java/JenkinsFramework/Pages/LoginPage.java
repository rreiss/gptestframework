package JenkinsFramework.Pages;

import JenkinsFramework.LoginData;
import org.apache.http.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;


/**
 * Created by rreiss on 9/1/2016.
 */
public class LoginPage   // for jenkins
{

    public  void goTo() {
        WebElement loginLink =  JenkinsFramework.Driver.instance.findElement(By.linkText("log in"));
        loginLink.click();

    }
    public void loginAsMe()
    {
          WebElement loginInput =  JenkinsFramework.Driver.instance.findElement(By.name("j_username"));
           // loginInput.click();
         loginInput.sendKeys("rebeccareiss");

          WebElement passwordInput =  JenkinsFramework.Driver.instance.findElement(By.name("j_password"));
           passwordInput.sendKeys("temp");
                  WebElement signinButton =  JenkinsFramework.Driver.instance.findElement(By.id("yui-gen1-button"));
           signinButton.click();
           checkLogin();
    }

    public void checkLogin()
    {
        //login  worked if there is log out on top right
        try {
         WebElement logoutButton = JenkinsFramework.Driver.instance.findElement(By.linkText("log out"));
            System.out.println("Login to Jenkins was successful!");
        } catch (NoSuchElementException e)
        {
            System.out.println("Login did not work.  Check username and password.");
            Asserts.check(false,"error with log in");
            e.printStackTrace();
        }
    }

}
