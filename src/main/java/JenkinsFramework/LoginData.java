package JenkinsFramework;

/**
 * Created by rreiss on 9/1/16
 */
public final class LoginData
{

    public static final String baseURL = "http://wildcat.jgi-psf.org:8989/jenkins";

    private LoginData(){}
}
