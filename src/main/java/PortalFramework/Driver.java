package PortalFramework;

//import PortalFramework.Pages.LoginPage;
//import PortalFramework.Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


/**
 * Created by ktaylor on 8/3/2015.
 */
public class Driver {
    public static WebDriver instance;
    public static WebDriverWait wait10max;
    public static WebDriverWait wait40max;

    public static JavascriptExecutor js;

    public static String downloadDir;
    public static String workingDir;

    public static void initialize() throws IOException {

        downloadDir = System.getProperty("java.io.tmpdir");
        System.out.println("Download directory: " + downloadDir);

        workingDir = System.getProperty("user.dir");

        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList",2); //this tells
        profile.setPreference("browser.download.dir", downloadDir);
        profile.setPreference("browser.download.manager.showWhenStarting",false);
        profile.setPreference("browser.download.manager.showAlertOnComplete",false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/excel, text/csv");


        File pathToBinary = new File("C:\\testing\\ff_45_2_0_esr\\firefox.exe");
        FirefoxBinary binary = new FirefoxBinary(pathToBinary);
        instance  = new FirefoxDriver(binary, profile);


        wait10max = new WebDriverWait(instance, 10);
        wait40max = new WebDriverWait(instance, 40);
        js = ((JavascriptExecutor) instance);

        instance.manage().window().maximize();

    }


    public static void close() {
        instance.quit();
    }

    public static void waitSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void noWait(Runnable fn) {
        turnOffWait();
        //return fn.apply(5, "foobar");
        fn.run();
        turnOnWait(5);
    }

    public static void turnOnWait(int secs) {
        instance.manage().timeouts().implicitlyWait(secs, TimeUnit.SECONDS);
    }

    public static void turnOffWait() {
        instance.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }


    public static void logout() {
        WebElement signout = instance.findElement(By.className("user-name"));

        Actions action = new Actions(instance);
        action.moveToElement(signout);
        action.perform();
        instance.findElement(By.id("logout")).click();
    }
}


