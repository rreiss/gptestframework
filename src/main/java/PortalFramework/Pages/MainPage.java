package PortalFramework.Pages;

import PortalFramework.LoginData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by rreiss on 9/1/2016.
 */
public class MainPage {


    public static String getTestURL() {
        return LoginData.testURL;
    }

    public static String getBaseURL() {
        return LoginData.baseURL;
    }
    public static String getBetaURL() {
        return LoginData.betaURL;
    }

    public  void goToTestSite() {
        PortalFramework.Driver.instance.get(getTestURL());
    }


    public  void goTo() {
        PortalFramework.Driver.instance.get(getBaseURL());
    }

    public  void goToBetaSite() {
        PortalFramework.Driver.instance.get(getBetaURL());
    }

    public  void goToMycocosm()
    {
        WebElement link = PortalFramework.Driver.instance.findElement(By.linkText("Mycocosm"));
        link.click();
    }


}
