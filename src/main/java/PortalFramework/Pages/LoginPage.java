package PortalFramework.Pages;

import PortalFramework.LoginData;
import org.apache.http.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.NoSuchElementException;




/**
 * Created by rreiss on 9/1/2016.
 */
public class LoginPage   // for Genome Portal
{

    public  void goTo() {
        WebElement loginLink =  PortalFramework.Driver.instance.findElement(By.id("login"));
        loginLink.click();

    }
    public void loginAsAdmin()
    {
            WebElement loginInput =  PortalFramework.Driver.instance.findElement(By.id("login"));
            loginInput.sendKeys(LoginData.admin_username);
            WebElement passwordInput =  PortalFramework.Driver.instance.findElement(By.id("password"));
            passwordInput.sendKeys(LoginData.admin_password);
            WebElement signinButton =  PortalFramework.Driver.instance.findElement(By.name("commit"));
            signinButton.click();
            checkLogin();
    }
    public void loginAsUser()
    {
        WebElement loginInput =  PortalFramework.Driver.instance.findElement(By.id("login"));
        loginInput.sendKeys(LoginData.username);
        WebElement passwordInput =  PortalFramework.Driver.instance.findElement(By.id("password"));
        passwordInput.sendKeys(LoginData.password);
        WebElement signinButton =  PortalFramework.Driver.instance.findElement(By.name("commit"));
        signinButton.click();
       checkLogin();
    }

    public void checkLogin()
    {
        //login did not work if there is no logout button on screen
        try {
         WebElement logoutButton = PortalFramework.Driver.instance.findElement(By.id("logout"));
        } catch (NoSuchElementException e) {
            System.out.println("Login did not work.  Check username and password.");
            Asserts.check(false,"error with log in");
            e.printStackTrace();
        }
    }

}
